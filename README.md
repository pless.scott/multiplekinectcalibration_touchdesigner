# MultipleKinectCalibration_TouchDesigner

A tool for calibration, recording and cloud point playback using multiple Azure Kinect sensors with TouchDesigner.

[Documentation](https://gitlab.com/v_brault/multiplekinectcalibration_touchdesigner/-/wikis/Multiple-Azure-Kinect-Calibration-Tool)